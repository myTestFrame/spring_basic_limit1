package com.xd.service.impl;

import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Administrator on 2019/10/24.
 * 1 计数器方式实现，限流
 */
@Service
public class LimterService {
    private static int limtCount = 10;// 限制最大访问的容量
    private static AtomicInteger atomicInteger = new AtomicInteger(0); // 每秒钟 实际请求的数量
    private static  long start = System.currentTimeMillis();// 获取当前系统时间
    private static  final int interval = 60*1000;// 间隔时间60秒

    public boolean acquire() {
        long newTime = System.currentTimeMillis();
        if (newTime > (start + interval)) {
            // 判断是否是一个周期
            start = newTime;
            atomicInteger.set(0); // 清理为0
            return true;

        }

        atomicInteger.incrementAndGet();// i++;
        System.out.println("atomicInteger"+atomicInteger.get());
        return atomicInteger.get() <= limtCount;
    }

    /**
     * 滑动窗口计数器
     * @return
     */


    static LimterService limitService = new LimterService();

    public static void main(String[] args) {

        ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();
        // 模拟100个请求
        for (int i = 1; i < 59; i++) {
            final int tempI = i;
            newCachedThreadPool.execute(new Runnable() {

                public void run() {

                    if (limitService.acquire()) {
                        System.out.println("你没有被限流,可以正常访问逻辑 i:" + tempI);
                    } else {
                        System.out.println("你已经被限流呢  i:" + tempI);
                    }
                }
            });
        }


    }


}
