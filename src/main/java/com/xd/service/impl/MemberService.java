package com.xd.service.impl;

import com.alibaba.fastjson.JSONObject;

import com.xd.util.HttpClientUtils;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2019/10/24.
 */
@Service
public class MemberService {
    public JSONObject getMember() {
        JSONObject result = HttpClientUtils.httpGet("http://127.0.0.1:8081/basic/member/memberIndex");
        return result;
    }

}
