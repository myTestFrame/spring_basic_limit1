package com.xd.controller;

import com.google.common.util.concurrent.RateLimiter;
import com.xd.service.impl.LimterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;

/**
 * Created by Administrator on 2019/10/24.
 */
@RestController
@RequestMapping("/limit")
public class LimitController {
    @Bean
    public ExecutorService getPool(){
        return Executors.newCachedThreadPool();
    }
    @Autowired
    ExecutorService executorService;
    @Autowired
    LimterService limterService;
    @RequestMapping("/limit1")
    public String limit1() throws ExecutionException, InterruptedException {
        String result="";
        if(limterService.acquire()){
            result =  "你没有被限流，可以正常访问";
        }else {
            result = "你已经被限流了，不能访问，放弃吧";
        }
        System.out.println(result);
        return result;
    }

    // 解释：1.0 表示 每秒中生成1个令牌存放在桶中
    RateLimiter rateLimiter = RateLimiter.create(2.0);

    //实现令牌桶限流
    @RequestMapping("/limit2")
    public String limit2(){
        // 下单请求
        // 1.限流判断
        // 如果在秒内 没有获取不到令牌的话，则会一直等待
     //   System.out.println("生成令牌等待时间:" + rateLimiter.acquire());
        boolean acquire = rateLimiter.tryAcquire(1000, TimeUnit.MILLISECONDS); // 等待时间是 1秒
        if (!acquire) {
            System.out.println(Thread.currentThread().getName()+"被限流");
            return "你在怎么抢，也抢不到，因为会一直等待的，你先放弃吧！";
        }else{
            System.out.println(Thread.currentThread().getName()+"顺利通过");
            return "没有限流可以抢购";
        }
    }


}
