package com.socket;

import org.junit.Test;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by Administrator on 2019/11/15.
 */
public class BufferTest3 {

    @Test
    public void test1() throws Exception {
        long start = System.currentTimeMillis();
        FileInputStream fis = new FileInputStream("D:\\buffer\\file1\\file1.mp4");
        FileOutputStream fos = new FileOutputStream("D:\\buffer\\file1\\file1.mp4");
        // ①获取通道
        FileChannel inChannel = fis.getChannel();
        FileChannel outChannel = fos.getChannel();
        // ②分配指定大小的缓冲区
        ByteBuffer buf = ByteBuffer.allocateDirect(1024);
        long middle = System.currentTimeMillis();
        System.out.println(middle-start);
        while (inChannel.read(buf) != -1) {
            buf.flip();// 切换为读取数据
            // ③将缓冲区中的数据写入通道中
            outChannel.write(buf);
            buf.clear();
        }
        outChannel.close();
        inChannel.close();
        fos.close();
        fis.close();
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test2() throws Exception {
        long start = System.currentTimeMillis();
        FileInputStream fis = new FileInputStream("D:\\buffer\\file1\\file1.mp4");
        FileOutputStream fos = new FileOutputStream("D:\\buffer\\file1\\file1.mp4");
        // ①获取通道
        FileChannel inChannel = fis.getChannel();
        FileChannel outChannel = fos.getChannel();
        // ②分配指定大小的缓冲区
        ByteBuffer buf = ByteBuffer.allocate(1024);
        long middle = System.currentTimeMillis();
        System.out.println(middle-start);
        while (inChannel.read(buf) != -1) {
            buf.flip();// 切换为读取数据
            // ③将缓冲区中的数据写入通道中
            outChannel.write(buf);
            buf.clear();
        }
        outChannel.close();
        inChannel.close();
        fos.close();
        fis.close();
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
    int length = 1000000;
    ByteBuffer[] buffers = new ByteBuffer[length];
    @Test
    public void allocate(){
        long start = System.currentTimeMillis();
        for (int i = 0; i < length; i++) {
            buffers[i] = ByteBuffer.allocate(1024);
        }

        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
    @Test
    public void allocateDirect(){
        long start = System.currentTimeMillis();
        for (int i = 0; i < length; i++) {
            buffers[i] = ByteBuffer.allocateDirect(1024);
        }

        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
    @Test
    public void randomAccess() throws Exception {
        long start = System.currentTimeMillis();
        RandomAccessFile raf1 = new RandomAccessFile("D:\\buffer\\file1\\file1.mp4", "rw");
        // 1.获取通道
        FileChannel channel = raf1.getChannel();
        // 2.分配指定大小的指定缓冲区
        ByteBuffer buf1 = ByteBuffer.allocate(1024);
        ByteBuffer buf2 = ByteBuffer.allocate(1024);
        // 3.分散读取
        ByteBuffer[] bufs = { buf1, buf2 };
        channel.read(bufs);
        for (ByteBuffer byteBuffer : bufs) {
            // 切换为读取模式
            byteBuffer.flip();
        }
        System.out.println(new String(bufs[0].array(), 0, bufs[0].limit()));
        System.out.println("------------------分算读取线分割--------------------");
        System.out.println(new String(bufs[1].array(), 0, bufs[1].limit()));
        // 聚集写入
        RandomAccessFile raf2 = new RandomAccessFile("D:\\buffer\\file1\\file1.mp4", "rw");
        FileChannel channel2 = raf2.getChannel();
        channel2.write(bufs);
        channel.close();
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }
}

