package com.socket;

import java.nio.ByteBuffer;

/**
 * Created by Administrator on 2019/11/14.
 */
public class BufferTest1 {
    public static void main(String[] args) {
        System.out.println("..........");
        // 1.指定缓冲区大小1024
        ByteBuffer buf = ByteBuffer.allocate(1024);
        System.out.println(buf.position()); // 数据初始位置
        System.out.println(buf.limit());    // 可用位置
        System.out.println(buf.capacity()); // 最大容量
        // 2.向缓冲区存放5个数据
        buf.put("abcd1".getBytes());
        System.out.println("--------------------");
        System.out.println(buf.position()); // 变为 5
        System.out.println(buf.limit());
        System.out.println(buf.capacity());
        // 3.开启读模式 flip 开启
        buf.flip();
        System.out.println("----------开启读模式...----------");
        System.out.println(buf.position());
        System.out.println(buf.limit());
        System.out.println(buf.capacity());
        byte[] bytes = new byte[buf.limit()];
        buf.get(bytes);
        System.out.println(new String(bytes, 0, bytes.length));
        System.out.println("----------重复读模式...----------");
        // 4.开启重复读模式
        buf.rewind();
        System.out.println(buf.position());
        System.out.println(buf.limit());
        System.out.println(buf.capacity());
        byte[] bytes2 = new byte[buf.limit()];
        buf.get(bytes2);
        System.out.println(new String(bytes2, 0, bytes2.length));
        // 5.clean 清空缓冲区  数据依然存在,只不过数据被遗忘，position 和limit 回到原来的位置
        System.out.println("----------清空缓冲区...----------");
        buf.clear();
        System.out.println(buf.position());
        System.out.println(buf.limit());
        System.out.println(buf.capacity());
        System.out.println((char)buf.get()); // 会唤醒数字
        System.out.println(buf.position());
        System.out.println(buf.limit());
        System.out.println(buf.capacity());
        buf.clear();
        /**
         * mark  和reset 模式
         * mark 在某个position做个标记，reset 的时候，positon 会回到这里
         */
        System.out.println("--------------- mark 和 reset 设置------------------");
        buf.put("abcde".getBytes());
        // 开启读取模式
        buf.flip();
        byte[] dst = new byte[buf.limit()];
        buf.get(dst, 0, 2);
        buf.mark();
        System.out.println(new String(dst, 0, 2));
        System.out.println(buf.position());
        buf.get(dst, 2, 2);
        System.out.println(new String(dst, 2, 2));
        System.out.println(buf.position());
        buf.reset();
        System.out.println("重置恢复到mark位置..");
        System.out.println(buf.position());


    }
}
