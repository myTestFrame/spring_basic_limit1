package com.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Administrator on 2019/11/14.
 */
public class TcpClient {
    public static void main(String[] args) throws IOException {
        System.out.println("socket tcp 客户端启动....");
        while (true){
            Socket socket = new Socket("127.0.0.1", 8080);
            OutputStream outputStream = socket.getOutputStream();
            System.out.println("请输入：");
            Scanner scanner = new Scanner(System.in);
            String d = scanner.nextLine();
            outputStream.write(d.getBytes());
            socket.close();
        }
    }
}
